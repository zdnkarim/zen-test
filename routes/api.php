<?php

use App\Http\Controllers\
{
    CategoryController,
    ProductController,
    VariantController,
    ImageController,
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'category'],function(){
    Route::get('/',[CategoryController::class,'getCategory']);
    Route::post('/',[CategoryController::class,'addCategory']);
    Route::get('/{id}',[CategoryController::class,'getCategoryById']);
    Route::patch('/{id}',[CategoryController::class,'editCategory']);
    Route::delete('/{id}',[CategoryController::class,'deleteCategory']);
});

Route::group(['prefix'=>'product'],function(){
   Route::get('/',[ProductController::class,'getProduct']);
   Route::post('/',[ProductController::class,'addProduct']);
   Route::get('/{id}',[ProductController::class,'getProductById']);
   Route::patch('/{id}',[ProductController::class,'editProduct']);
   Route::delete('/{id}',[ProductController::class,'deleteProduct']);
});

Route::group(['prefix'=>'variant'],function(){

    Route::get('/image',[ImageController::class,'getImage']);
    Route::post('/image',[ImageController::class,'addImage']);
    Route::delete('/image/{id}',[ImageController::class,'deleteImage']);

    Route::get('/',[VariantController::class,'getVariant']);
    Route::post('/',[VariantController::class,'addVariant']);
    Route::get('/{id}',[VariantController::class,'getVariantById']);
    Route::patch('/{id}',[VariantController::class,'editVariant']);
    Route::delete('/{id}',[VariantController::class,'deleteVariant']);
});
