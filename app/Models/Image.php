<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $table = 'product_images';
    protected $fillable = ['image_url','variant_id'];

    public function variant(){
        return $this->belongsTo(Variant::class);
    }

}
