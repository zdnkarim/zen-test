<?php

namespace App\Helpers;

class ResponseHelper{
    public static function success($status, $data,$message){
        $response = [
          'data'=>$data,
          'message'=>$message 
        ];
        return response()->json($response,$status);
    }
    public static function error($status, $message){
        $response = [
            'data'=>$message,
            'message'=>'error'
        ];
        return response()->json($response,$status);

    }
};