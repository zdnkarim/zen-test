<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\{
    Image,
    Variant
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    
    function responseFormat($image){
        return [
            'id'=> $image->id,
            'variant'=> [
                'id'=> $image->variant->id,
                'name'=> $image->variant->name,
            ],
            'url'=> $image->image_url
        ];
    }

    function getImage(Request $request){
        $variant_id = $request->query('variant_id');
        try {
            if($variant_id){
                $images = Image::where('variant_id',$variant_id)->get();
            }else{
                $images = Image::all();
            }
            if(!$images->count())
                return ResponseHelper::error(404, 'Image is empty');

            $response = $images->map(function($image){
                return $this->responseFormat($image);
            });

            return ResponseHelper::success(200,$response,'Successfully get images');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function addImage(Request $request){

        $validator = Validator::make($request->all(),[
            'image_file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:4500',
            'variant_id'=> 'required|integer'
        ]);

        if($validator->fails())
            return ResponseHelper::error(400,$validator->errors());

        $image_file = $request->file('image_file');
        $variant_id = $request->input('variant_id');
        try {
            $variant = Variant::find($variant_id);
            if(!$variant)
                return ResponseHelper::error(404,'Variant with id='.$variant_id.' does not exists');

            $imageName = time().'.'.$image_file->extension();
            $imagePath = url('/').'/images/'.$imageName;
            $image_file->move(public_path('images'),$imageName);

            $image = Image::create([
                'image_url'=>$imagePath,
                'variant_id'=>$variant_id,
            ]);

            $response = $this->responseFormat($image);

            return ResponseHelper::success(201,$response,'Successfully add new image');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function deleteImage($id){
        try {
            $image = Image::find($id);
            if(!$image)
                return ResponseHelper::error(404, 'Image with id='.$id.' does not exists');
            
            $imageName = explode('/',$image->image_url)[4];
            
            $response = $image->delete();
            unlink(public_path('images/').$imageName);

            $newResponse = [
                "id" => $id,
                'isDeleted'=>$response
            ];
            
            return ResponseHelper::success(200,$newResponse,'Successfully deleted image by id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
}
