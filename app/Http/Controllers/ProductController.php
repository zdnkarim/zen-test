<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\{
    Category,
    Product
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    function responseFormat($product){
        return [
            'id'=>$product->id,
            'name'=>$product->name,
            'category'=>[
                'id'=>$product->category->id,
                'name'=>$product->category->name,
            ],
            'total_stock'=>$product->total_stock
        ];  
    }

    function getProduct(Request $request){
        $category_id = $request->query('category_id');
        try {
            if($category_id){
                $products = Product::where('category_id',$category_id,)
                ->get();
            }else{
                $products = Product::all();
            }

            if(!$products->count())
                return ResponseHelper::error(404,'Products is empty');

            $response = $products->map(function($product){
                return $this->responseFormat($product);
            });

            return ResponseHelper::success(200,$response,'Successfully get products');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function addProduct(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required|integer',
        ]);

        if ($validator->fails())
            return ResponseHelper::error(400,$validator->errors());

        $name = strtolower($request->input('name'));
        $category_id = $request->input('category_id');

        try {
            $category = Category::find($category_id);
            if(!$category)
                return ResponseHelper::error(404,"Category with id=".$category_id. ' does not exists');

            $data = Product::where('name',$name)->where('category_id',$category_id)->first();
            if($data)
                return ResponseHelper::error(400,$name.' already exists in product data');
            
            $product = Product::create([
                'name'=>$name,
                'total_stock'=>0,
                'category_id'=>$category_id,
            ]);

            $response=$this->responseFormat($product);

            return ResponseHelper::success(201, $response,'Successfully add new product');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }

    }
    function getProductById($id){
        try {
            $product = Product::find($id);
            if(!$product)
                return ResponseHelper::error(404,'Product with id='.$id.' does not exists');

            $response =  $this->responseFormat($product);

            return ResponseHelper::success(200,$response,'Successfully get product by id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }

    function editProduct(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'category_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::error(400,$validator->errors());
        }

        try {
            $product = Product::find($id);
            if(!$product)
                return ResponseHelper::error(404,'Product with id='.$id.' does not exists');
            
            $name = strtolower($request->input('name')) ?: $product->name;
            $category_id = $request->input('category_id');

            $category = Category::find($category_id);
            if(!$category)  
                return ResponseHelper::error(404,"Category with id=".$category_id. ' does not exists');
            
            $data = Product::where('name',$name)->where('category_id',$category_id)->first();
            if($data)
                return ResponseHelper::error(400,$name.' already exists in product data');

            $response = $product->update([
                'name'=>$name,
                'category_id'=>$category_id,
            ]);

            $newResponse=[
                array_merge(
                    $this->responseFormat($product),
                    ['isUpdated'=>$response],
                )
            ];
            return ResponseHelper::success(200,$newResponse[0],'Successfully updated product with id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function deleteProduct($id){
        try {
            $product = Product::find($id);
            if(!$product)
                return ResponseHelper::error(404,'Product with id='.$id.' does not exists');
            
            $response = $product->delete();
            
            $newResponse =  [
                'id'=>$id,
                'isDeleted'=>$response
            ];  
            return ResponseHelper::success(200,$newResponse,'Successfully deleted product by id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
}
