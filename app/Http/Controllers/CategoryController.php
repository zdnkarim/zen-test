<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Category;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    
    function getCategory(){
        try {
            $data = Category::select('id','name')->orderBy('id','ASC')->get();

            if(!$data->count()) return ResponseHelper::error(404,'Categories is empty');
            
            return ResponseHelper::success(200,$data,'Successfully get categories');
            
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }

    function addCategory(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::error(400,$validator->errors());
        }

        $name = strtolower($request->input('name'));
        
        try {
            $data = Category::where('name',$name)->first();
            if($data)
                return ResponseHelper::error(400,$name.' already exists in category data');

            $response = Category::create([
                'name' => $name,
            ])->only('id','name');
            
            return ResponseHelper::success(201, $response,'Successfully add new category');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }

    function getCategoryById($id){
        try {
            $category = Category::select('id','name')->find($id);
            if(!$category)
                return ResponseHelper::error(404,'Category with id='.$id.' does not exists');
            
            return ResponseHelper::success(200,$category,'Successfully get category by id='.$id);
            
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }

    function editCategory(Request $request, $id){
        $name = strtolower($request->input('name'));
        
        try {
            $category = Category::select('id','name')->find($id);

            if(!$category)
                return ResponseHelper::error(404,'Category with id='.$id.' does not exists');

            if(empty($name) || $name === $category->name)
                return ResponseHelper::success(200,$category,'There is no change saved');
            
            $data = Category::where('name',$name)->first();
            
            if($data && $data->id !== $id)
                return ResponseHelper::error(400,$name.' already exists in category data');
            
            $response = $category->update([
                'name'=>$name,
            ]);

            $newResponse = [
                'id'=>$id,
                'name' => $name,
                'isUpdated' => $response,
            ];
            return ResponseHelper::success(200,$newResponse,'Successfully updated category with id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }

    function deleteCategory($id){
        try {
            $category = Category::find($id);
            if(!$category)
                return ResponseHelper::error(404,'Category with id='.$id.' does not exists');
            
            $response = $category->delete();
            $newResponse = [
                'id'=>$id,
                'isDeleted' => $response,
            ];
            return ResponseHelper::success(200,$newResponse,'Successfully get category by id='.$id);
            
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
}
