<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\{
    Product,
    Variant
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Whoops\Handler\XmlResponseHandler;

class VariantController extends Controller
{

    function responseFormat($variant){
        return[
            'id'=>$variant->id,
            'name'=>$variant->name,
            'product'=>[
                'id'=>$variant->product->id,
                'name'=>$variant->product->name,
            ],
            'price'=>$variant->price,
            'stock'=>$variant->stock
        ];
    }

    function getVariant(Request $request){
        $product_id = $request->query('product_id');
        try {
            if($product_id){
                $variants = Variant::where('product_id',$product_id)
                ->get();
            }else{
                $variants = Variant::all();
            }

            if(!$variants->count())
                return ResponseHelper::error(404,'Variants is empty');

            $response = $variants->map(function($variant){
                return $this->responseFormat($variant);
            });

            return ResponseHelper::success(200,$response,'Successfully get variants');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function addVariant(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'product_id' => 'required|integer'
        ]);

        if($validator->fails())
            return ResponseHelper::error(400,$validator->errors());

        $name = strtolower($request->input('name'));
        $stock = $request->input('stock');
        $product_id = $request->input('product_id');
        $price = $request->input('price');
        
        try {
            $product = Product::find($product_id);
            if(!$product)
                return ResponseHelper::error(404,"Product with id=".$product_id. ' does not exists');

            $data = Variant::where('name',$name)->where('product_id',$product_id)->first();
            if($data)
                return ResponseHelper::error(400,$name." already exists in variants data");

            $variant = Variant::create([
                'name'=>$name,
                'product_id'=>$product_id,
                'price'=>$price,
                'stock'=>$stock,
            ]);

            $product->update([
                'total_stock'=>$product->total_stock + $stock
            ]);

            $response=$this->responseFormat($variant);

            return ResponseHelper::success(201, $response,'Successfully add new variant');
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }

    }
    function getVariantById($id){
        try {
            $variant = Variant::find($id);
            if(!$variant)
                return ResponseHelper::error(404,'Variant with id='.$id." does not exists");

            $response = $this->responseFormat($variant);

            return ResponseHelper::success(200,$response,'Successfully get variant by id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function editVariant(Request $request, $id){

        $validator = Validator::make($request->all(),[
            'product_id' => 'required|integer'
        ]);

        if($validator->fails())
            return ResponseHelper::error(400,$validator->errors());

        try {
            $variant = Variant::find($id);
            if(!$variant)
                return ResponseHelper::error(404, 'Variant data with id='.$id.' does not exists');
            
            $name = strtolower($request->input('name')) ?: $variant->name;
            $stock = $request->input('stock') ?? $variant->stock;
            $product_id = $request->input('product_id');
            $price = $request->input('price') ?? $variant->price;
            
            $product = Product::find($product_id);
            if(!$product)
                return ResponseHelper::error(404,"Product with id=".$product_id." does not exists");

            $data = Variant::where('name',$name)->where('product_id',$product_id)->first();
            if($data && $data->id != $id)
                return ResponseHelper::error(400,$name." already exists in product data");

            $totalStock = $stock - $variant->stock;

            $response = $variant->update([
                'name'=>$name,
                'stock'=>$stock,
                'product_id'=>$product_id,
                'price'=>$price
            ]);

            $product->update([
                'total_stock'=>$product->total_stock + $totalStock
            ]);

            $newResponse = [
                array_merge(
                    $this->responseFormat($variant),
                    ['isUpdated'=>$response]
                )
            ];

            return ResponseHelper::success(200,$newResponse[0],'Successfully updated product with id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
    function deleteVariant($id){
        try {
            $variant = Variant::find($id);
            if(!$variant)
                return ResponseHelper::error(404,'Variant with id='.$id." does not exists");

            $stock = $variant->stock;
            $productId = $variant->product_id;
            $product = Product::find($productId);
            
            $response = $variant->delete();

            $product->update([
                'total_stock'=>$product->total_stock - $stock
            ]);

            $newResponse = [
                'id' => $id,
                'isDeleted'=>$response
            ];
            return ResponseHelper::success(200,$newResponse,'Successfully deleted variant by id='.$id);
        } catch (\Throwable $th) {
            if(empty($th)) return ResponseHelper::error(500,'An unknwon error occured');
            return ResponseHelper::error(400,$th->getMessage());
        }
    }
}
