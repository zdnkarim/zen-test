<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        
        Schema::create('product_categories',function(Blueprint $table){
            $table->id();
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::create('products',function(Blueprint $table){
            $table->id();
            $table->string('name');
            $table->integer('total_stock');
            $table->bigInteger('category_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');

        });
        Schema::create('variants',function(Blueprint $table){
            $table->id();
            $table->string('name');
            $table->bigInteger('product_id')->unsigned()->index();
            $table->integer('price');
            $table->integer('stock');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
        Schema::create('product_images',function(Blueprint $table){
            $table->id();
            $table->string('image_url');
            $table->bigInteger('variant_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('variant_id')->references('id')->on('variants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
